package main

import (
	"gitlab.com/softwise/internal/app"
	"gitlab.com/softwise/internal/app/telegram"
	"gitlab.com/softwise/pkg/log"
	"os"
)

func main() {
	configuration := &app.Configuration{}
	configuration.Load("config.yaml")

	if configuration.BotToken == "" {
		log.Fatal("Bot token is required")
		os.Exit(1)
	}

	if configuration.ChatId == 0 {
		log.Fatal("Chat id is required")
		os.Exit(1)
	}

	log.Info("Started")
	log.Info("Environment: ", configuration.Environment)

	telegramBot := &telegram.Bot{
		Configuration: configuration,
	}

	telegramBot.Start()
}
