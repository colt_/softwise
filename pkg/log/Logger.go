package log

import (
	"github.com/sirupsen/logrus"
)

const (
	PanicLevel logrus.Level = iota
	FatalLevel
	ErrorLevel
	WarnLevel
	InfoLevel
	DebugLevel
)

var logger = &Logger{}

type Logger struct {
	loggers []*logrus.Logger
}

func New() *logrus.Logger {
	return logrus.New()
}

func AddLogger(specificLogger *logrus.Logger) {
	logger.loggers = append(logger.loggers, specificLogger)
}

func AddLoggers(specificLoggers ...*logrus.Logger) {
	logger.loggers = append(logger.loggers, specificLoggers...)
}

func Debug(args ...interface{}) {
	Log(DebugLevel, args...)
}

func Debugf(format string, args ...interface{}) {
	Logf(DebugLevel, format, args...)
}

func Info(args ...interface{}) {
	Log(InfoLevel, args...)
}

func Infof(format string, args ...interface{}) {
	Logf(InfoLevel, format, args...)
}

func Warning(args ...interface{}) {
	Log(WarnLevel, args...)
}

func Warningf(format string, args ...interface{}) {
	Logf(WarnLevel, format, args...)
}

func Error(args ...interface{}) {
	Log(ErrorLevel, args...)
}

func Errorf(format string, args ...interface{}) {
	Logf(ErrorLevel, format, args...)
}

func Fatal(args ...interface{}) {
	Log(FatalLevel, args...)
}

func (logger *Logger) Fatalf(format string, args ...interface{}) {
	Logf(FatalLevel, format, args...)
}

func Log(level logrus.Level, args ...interface{}) {
	for _, specificLogger := range logger.loggers {
		specificLogger.Log(level, args...)
	}
}

func Logf(level logrus.Level, format string, args ...interface{}) {
	for _, specificLogger := range logger.loggers {
		specificLogger.Logf(level, format, args...)
	}
}
