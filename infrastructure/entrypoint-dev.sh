#!/bin/sh

# local configuration
if [ -f configs/config-local.yaml ]; then
    echo "applying local configuration"
    cp configs/config-local.yaml config.yaml
else
    echo "applying default dev configuration"
    cp configs/config-dev.yaml config.yaml
fi

glide install
go run main.go
