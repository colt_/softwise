package telegram

import (
	"encoding/csv"
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/softwise/internal/app"
	"gitlab.com/softwise/pkg/log"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"reflect"
	"strconv"
)

var NumberCoroutines int

type Bot struct {
	Configuration *app.Configuration
}

type URL struct {
	Source           string
	NumberCharacters int
}

func (telegramBot *Bot) Start() {
	// Using the token, create a new bot instance
	bot, err := tgbotapi.NewBotAPI(telegramBot.Configuration.BotToken)
	if err != nil {
		panic(err)
	}

	log.Infof("Authorized on account %s", bot.Self.UserName)

	// Initialize the channel where updates from the API will come
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60
	updates, err := bot.GetUpdatesChan(u)

	for update := range updates {
		// Check on empty message
		if update.Message == nil {
			continue
		}

		// Check on commands
		if reflect.TypeOf(update.Message.Text).Kind() == reflect.String && update.Message.Text != "" && update.Message.Document == nil {
			switch update.Message.Command() {
			case "start":
				msg := tgbotapi.NewMessage(telegramBot.Configuration.ChatId, "Привет, я telegram-бот на Golang, "+
					"который на вход получает количество используемых потоков и файл со списком URL, дергает эти URL методом GET, "+
					"в теле ответа считает количество символов и возвращает результат.\n\n"+
					"Какое количество потоков использовать? (используйте команду [numberCoroutines %d]) :) ")
				bot.Send(msg)

				log.Info(msg)
			case "numberCoroutines":
				i, err := strconv.Atoi(update.Message.CommandArguments())

				if err != nil {
					msg := tgbotapi.NewMessage(telegramBot.Configuration.ChatId, "Введите целое число -_-")
					bot.Send(msg)

					log.Error(msg)
					continue
				}

				NumberCoroutines = i

				msg := tgbotapi.NewMessage(telegramBot.Configuration.ChatId, "Спасибо. Теперь загрузите файл в формате CSV со списком URL, "+
					"где будет только одна колонка и в каждом отдельном столбце будет располагаться Ваш URL :)")
				bot.Send(msg)

				log.Info(msg)
			}

			// Check file download
		} else if update.Message.Document != nil && NumberCoroutines != 0 {
			// @TODO allowed CSV file
			// Check on format file
			if update.Message.Document.MimeType != "text/csv" {
				msg := tgbotapi.NewMessage(telegramBot.Configuration.ChatId, "Допускаются только файлы в формате CSV :/")
				bot.Send(msg)

				log.Error(msg)
				continue
			}

			// Get link file by FileID
			file, err := bot.GetFileDirectURL(update.Message.Document.FileID)
			if err != nil {
				log.Error(err)
				continue
			}

			log.Infof("Source link: %s", file)

			data, err := http.Get(file)
			// Parse the csv file
			reader := csv.NewReader(data.Body)

			var urls []string

			// Iterate through the records
			for {
				record, err := reader.Read()
				// Check validation CVS file
				if err == io.EOF {
					break
				} else if err != nil {
					log.Error(err)
					break
				}

				// Check URL validation
				if isUrl(record[0]) == false {
					continue
				}

				// Append urls
				urls = append(urls, record[0])
			}

			log.Info(urls)

			// Create chunk for urls
			var chunkUrls [][]string
			chunkSize := (len(urls) + NumberCoroutines - 1) / NumberCoroutines
			for i := 0; i < len(urls); i += chunkSize {
				end := i + chunkSize

				if end > len(urls) {
					end = len(urls)
				}

				chunkUrls = append(chunkUrls, urls[i:end])
			}

			// Check on empty slice
			if len(chunkUrls) == 0 {
				log.Error("Список URL пустой")
				continue
			} else if len(chunkUrls) < NumberCoroutines {
				msg := tgbotapi.NewMessage(telegramBot.Configuration.ChatId, "Количество потоков должно быть меньше для этого списка URL :/")
				bot.Send(msg)

				log.Error(msg)
				continue
			}

			c := make(chan URL)

			// Get number of characters
			for i := 0; i < NumberCoroutines; i++ {
				log.Info(chunkUrls[i])
				go runeCalculator(c, chunkUrls[i])
			}

			// Send results
			go func() {
				for {
					url := <-c
					text := fmt.Sprintf("%s - %d", url.Source, url.NumberCharacters)
					msg := tgbotapi.NewMessage(telegramBot.Configuration.ChatId, text)
					bot.Send(msg)
				}
			}()
		}
	}

}

func runeCalculator(c chan<- URL, urls []string) {
	for _, element := range urls {
		res, err := http.Get(element)
		if err != nil {
			log.Fatal(err)
			return
		}

		content, err := ioutil.ReadAll(res.Body)
		res.Body.Close()
		if err != nil {
			log.Fatal(err)
			return
		}

		numberCharacters := len([]rune(string(content)))
		log.Infof("Для URL - %s, количество возвращаемых символов равняется - %d", element, numberCharacters)

		// @TODO space and other special characters are considered symbols
		c <- URL{Source: element, NumberCharacters: numberCharacters}
	}
}

func isUrl(str string) bool {
	u, err := url.Parse(str)
	return err == nil && u.Scheme != "" && u.Host != ""
}
