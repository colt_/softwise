package app

import (
	"gitlab.com/softwise/pkg/log"
	"gopkg.in/natefinch/lumberjack.v2"
	"gopkg.in/yaml.v2"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
)

type Configuration struct {
	Environment string `yaml:"environment"`
	BotToken    string `yaml:"bot_token"`
	ChatId      int64  `yaml:"chat_id"`
}

func (configuration *Configuration) Load(configurationPath string) {
	filename, _ := filepath.Abs(configurationPath)
	configurationFile, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}

	err = yaml.Unmarshal(configurationFile, configuration)
	if err != nil {
		panic(err)
	}

	configuration.configureLoggers()

	log.Info("Configuration loaded")
}

func (configuration *Configuration) configureLoggers() {
	overallLogger := log.New()
	overallLogger.SetLevel(log.InfoLevel)
	overallLogger.SetFormatter(&log.TextFormatter{})
	overallLogger.SetOutput(
		io.MultiWriter(
			os.Stdout,
			&lumberjack.Logger{},
		),
	)

	log.AddLoggers(overallLogger)
}
